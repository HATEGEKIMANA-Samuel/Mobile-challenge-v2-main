# Project Name
To-Do App

# Description
The main purpose of this application is to provide yourself with a list of your priorities in order to ensure that you don't forget anything and are able to effectively plan out your tasks so that they are all accomplished in the correct time frame.

# Technologies used
  **programming language:** Flutter with dart

  **Data storage:** Hive db

# Android apk can be found in 
  to_do_app/build/app/outputs/flutter-apk

# Installation

**Environment**:

minmum sdk version 21

Adroid: 4.5


**Run project locally**

  Clone the project: [link](https://gitlab.com/HATEGEKIMANA-Samuel/Mobile-challenge-v2-main.git)

  Run flutter pub get to install dependences

  Run flutter run to start the app

**To test project**

  **IntelliJ**

  - Open the file you want to test

  - Select the Run menu

  - Click the Run 'tests in file' option.

  **VSCode**

  - Open the file you want to test

  - Select the Run menu

  - Click the Start Debugging option.

You can also use a terminal to run the tests by executing the following command from the root of the project:
  
  flutter test test/filename

# Contributor
  Samuel HATEGEKIMANA

# Copyright
  Alright reselved.
